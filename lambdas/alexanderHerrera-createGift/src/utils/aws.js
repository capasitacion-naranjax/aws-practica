import { DynamoDBClient, UpdateItemCommand } from '@aws-sdk/client-dynamodb'

const region = process.env.REGION || 'us-east-2'
const table = process.env.DB_TABLE

const dynamoClient = new DynamoDBClient({ region })

export const createGift = async (user, gift) => {
  console.log('Creating gift on database')
  console.log(`we will send: ${gift} to ${user.dni}`)
  // await dynamoClient.send(new PutItemCommand({
  //   TableName: table,
  //   Item: {
  //     dni: {
  //       S: user.dni
  //     },
  //     giftType: {
  //       S: gift
  //     }
  //   }
  // }
  // ))

  const giftAdded = await dynamoClient.send(new UpdateItemCommand({
    TableName: table,
    Key: {
      dni: { S: user.dni }
    },
    UpdateExpression: 'SET gift = :newGift',
    ExpressionAttributeValues: {
      ':newGift': { S: gift }
    },
    ReturnValues: 'ALL_NEW'
  }))
  console.log('Gift created.')
  console.log(giftAdded?.Attributes)
}
