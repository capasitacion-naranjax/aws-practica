const giftPerSeason = {
  fall: 'buzo',
  winter: 'sweater',
  spring: 'camisa',
  summer: 'ramera'
}

const getSeason = (birthDate) => {
  const months = new Date(birthDate).getMonth()

  if (months >= 2 && months <= 4) {
    return 'spring'
  } else if (months >= 5 && months <= 7) {
    return 'summer'
  } else if (months >= 8 && months <= 10) {
    return 'fall'
  } else {
    return 'winter'
  }
}

export const giftDecider = (birthDate) => {
  const season = getSeason(birthDate)
  console.log(`User will receive: ${giftPerSeason[season]}`)
  return giftPerSeason[season]
}
