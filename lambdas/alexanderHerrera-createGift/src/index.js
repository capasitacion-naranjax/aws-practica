import { giftDecider } from './utils/gifts.js'
import { createGift } from './utils/aws.js'

export const handler = async (event) => {
  try {
    const snsMessage = JSON.parse(event.Records[0].body)
    const userData = JSON.parse(snsMessage.Message)
    console.log('User Received: ', userData)

    const gift = giftDecider(userData.birthDate)
    await createGift(userData, gift)
    return {
      status: 200,
      success: true,
      lambda: 'Gift created successfully',
      gift
    }
  } catch (error) {
    console.error('We had some problem creating gift', error)
    return {
      success: false,
      status: error.status,
      error_message: error.message
    }
  }
}
