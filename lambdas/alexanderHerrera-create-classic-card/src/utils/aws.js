import { DynamoDBClient, UpdateItemCommand } from '@aws-sdk/client-dynamodb'

const region = process.env.REGION || 'us-east-2'
const table = process.env.DB_TABLE

const dynamoClient = new DynamoDBClient({ region })

export const insertCard = async (user, cardGenerated) => {
  console.log('Atempting to insert classic card on database')
  // await dynamoClient.send(new PutItemCommand({
  //   TableName: table,
  //   Item: {
  //     dni: {
  //       S: user.dni
  //     },
  //     number: {
  //       S: cardGenerated.number
  //     },
  //     expiration: {
  //       S: cardGenerated.expiration
  //     },
  //     type: {
  //       S: cardGenerated.type
  //     },
  //     csv: {
  //       S: cardGenerated.csv
  //     }
  //   }
  // }
  // ))

  const cardInsertResult = await dynamoClient.send(new UpdateItemCommand({
    TableName: table,
    Key: {
      dni: {
        S: user.dni
      }
    },
    UpdateExpression: 'SET #number = :number, #expiration = :expiration, #type = :type, #cvv = :cvv',
    ExpressionAttributeNames: {
      '#number': 'number',
      '#expiration': 'expiration',
      '#type': 'type',
      '#cvv': 'cvv'
    },
    ExpressionAttributeValues: {
      ':number': { S: cardGenerated.number },
      ':expiration': { S: cardGenerated.expiration },
      ':type': { S: cardGenerated.type },
      ':cvv': { S: cardGenerated.cvv }
    },
    ReturnValues: 'ALL_NEW'
  }))
  console.log('Card created.')
  console.log(cardInsertResult?.Attributes)
}
