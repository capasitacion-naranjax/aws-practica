import { SQSClient, SendMessageCommand } from '@aws-sdk/client-sqs'

const region = process.env.REGION

const sqsClient = new SQSClient({ region })

const getQueueUrl = (age) => {
  const classicCardQueue = process.env.SQS_CLASSIC_CARD
  const goldCardQueue = process.env.SQS_GOLD_CARD

  if (age <= 45) {
    console.log('User will receive a classic card')
    return classicCardQueue
  } else if (age > 45) {
    console.log('User will receive a gold card')
    return goldCardQueue
  }
}

export const sendMessageToSqs = async (userdata, age) => {
  const url = getQueueUrl(age)
  const params = {
    QueueUrl: url,
    MessageBody: JSON.stringify(userdata)
  }
  await sqsClient.send(new SendMessageCommand(params))
}
