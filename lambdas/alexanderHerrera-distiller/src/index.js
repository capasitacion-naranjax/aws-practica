import { getAge } from './utls/manageAge.js'
import { sendMessageToSqs } from './utls/aws.js'

export const handler = async (event) => {
  try {
    const snsMessage = JSON.parse(event.Records[0].body)
    const userData = JSON.parse(snsMessage.Message)
    console.log('User Data received', userData)

    const age = getAge(userData.birthDate)
    console.log('User age: ', age)
    await sendMessageToSqs(userData, age)
    return {
      status: 200,
      success: true,
      lambda: 'User sent to card creation'
    }
  } catch (error) {
    console.error('Something went wrong while trying to send users to the card creation lambda', error)
    return {
      status: 500,
      success: false,
      error_message: error.message
    }
  }
}
