import { generateCard } from './utils/card.js'
import { insertCard } from './utils/aws.js'

export const handler = async (event) => {
  try {
    const userData = JSON.parse(event.Records[0].body)
    console.log('User data: ', userData)
    const cardGenerated = generateCard()
    console.log('card data: ', cardGenerated)
    await insertCard(userData, cardGenerated)

    return {
      status: 200,
      success: true,
      lambda: 'Gold card created successfully'
    }
  } catch (error) {
    console.error('Error creating gold card', error)
    return {
      status: 500,
      success: false,
      error_message: error.message
    }
  }
}
