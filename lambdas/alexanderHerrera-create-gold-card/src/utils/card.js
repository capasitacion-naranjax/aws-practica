const generateCardNumber = () => {
  let creditCardNumber = ''

  for (let i = 1; i <= 16; i++) {
    const randomDigit = Math.floor(Math.random() * 10)
    creditCardNumber = creditCardNumber.concat(randomDigit)

    if (i % 4 === 0 && i < 16) {
      creditCardNumber.concat('-')
    }
  }

  return creditCardNumber
}

const generateExpirationDate = () => {
  const currentDate = new Date()
  currentDate.setFullYear(currentDate.getFullYear() + 2)

  const year = currentDate.getFullYear()
  const month = currentDate.getMonth() + 1
  const formattedMonth = month < 10 ? `0${month}` : month
  return `${formattedMonth}/${year}`
}

const generateCvv = () => {
  const min = 100
  const max = 999
  return (Math.floor(Math.random() * (max - min + 1)) + min).toString()
}

export const generateCard = () => {
  const cardNumber = generateCardNumber()
  const expirationDate = generateExpirationDate()
  const cvv = generateCvv()
  return {
    number: cardNumber,
    expiration: expirationDate,
    cvv,
    type: 'gold'
  }
}
