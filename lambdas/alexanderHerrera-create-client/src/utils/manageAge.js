const getAge = (birthDate) => {
  // "06/24/2008"
  const dob = new Date(birthDate)
  const monthDiff = Date.now() - dob.getTime()
  const ageDF = new Date(monthDiff)
  const year = ageDF.getUTCFullYear()
  return Math.abs(year - 1970)
}

export const validateAge = (birthDate) => {
  const age = getAge(birthDate)
  return age > 18 && age < 65
}
