import { DynamoDBClient, PutItemCommand } from '@aws-sdk/client-dynamodb'
import { PublishCommand, SNSClient } from '@aws-sdk/client-sns'

const region = process.env.REGION || 'us-east-2'
const table = process.env.DB_TABLE
const snsArn = process.env.SNS_ARN

const dynamoClient = new DynamoDBClient({ region })
const snsClient = new SNSClient({ region })

export const createUser = async (user) => {
  try {
    console.log(region, table, snsArn)
    const userCreated = await dynamoClient.send(new PutItemCommand({
      TableName: table,
      Item: {
        dni: {
          S: user.dni
        },
        name: {
          S: user.name
        },
        lastName: {
          S: user.lastName
        },
        birthDate: {
          S: user.birthDate
        }
      },
      ReturnValues: 'ALL_OLD'
    }))
    console.log(`User creation status code: ${userCreated?.$metadata?.httpStatusCode}`)
  } catch (error) {
    console.error('Something went wrong while creating user', error)
    throw error
  }
}

export const sendUserMessage = async (user) => {
  await snsClient.send(new PublishCommand({
    Message: JSON.stringify(user),
    TopicArn: snsArn
  }))
}
