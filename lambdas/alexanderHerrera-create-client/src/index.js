import { validateAge } from './utils/manageAge.js'
import { createUser, sendUserMessage } from './utils/aws.js'

export const handler = async (event) => {
  try {
    const user = event

    if (!validateAge(user.birthDate)) {
      console.error('User not valid to receive a card', user.birthDate)
      return {
        success: 200,
        created: false
      }
    }
    await createUser(user)
    await sendUserMessage(user)
    return {
      status: 200,
      success: true,
      message: 'User created successfully',
      user
    }
  } catch (error) {
    return {
      status: 200,
      success: false,
      error_message: error.message
    }
  }
}
